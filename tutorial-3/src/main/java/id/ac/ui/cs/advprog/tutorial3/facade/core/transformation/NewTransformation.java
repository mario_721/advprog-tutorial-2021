package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import org.springframework.stereotype.Component;

/**
 * Kelas ini melakukan cipher transposisi rotasi
*/
@Component
public class NewTransformation {
    private int key;

    public NewTransformation(int key){
        this.key = key;
    }

    public NewTransformation(){
        this(3);
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? -1 : 1;
        int n = text.length();
        char[] res = new char[n];
        for(int i = 0; i < n; i++){
            int newIdx = i + key * selector;
            newIdx = newIdx < 0 ? n + newIdx : newIdx % n;
            res[i] = text.charAt(newIdx);
        }

        return new Spell(new String(res), codex);
    }
}
