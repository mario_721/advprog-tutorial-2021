package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
//        List<Weapon> allWeapon = weaponRepository.findAll();
        List<Bow> bowWeapon = bowRepository.findAll();
        List<Spellbook> spellbookWeapon = spellbookRepository.findAll();

        for (Bow i : bowWeapon){
            Weapon bowAdapter = new BowAdapter(i);
            if(weaponRepository.findAll().contains(i)){
                continue;
            }
            else{
                weaponRepository.save(bowAdapter);
            }
//            allWeapon.add(bowAdapter);
        }

        for (Spellbook i : spellbookWeapon){
            Weapon spellAdapter = new SpellbookAdapter(i);
//            allWeapon.add(spellAdapter);
            if(weaponRepository.findAll().contains(i)){
                continue;
            }
            else {
                weaponRepository.save(spellAdapter);
            }
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        Bow bow = bowRepository.findByAlias(weaponName);
        Spellbook spellbook = spellbookRepository.findByAlias(weaponName);

        if  (spellbook!=null){
            weapon = new SpellbookAdapter(spellbook);
        }
        else if (bow!=null){
            weapon = new BowAdapter(bow);
        }

        String theAttackType;
        String theAttack;
        if(attackType==0){
            theAttackType = "normal attack";
            theAttack = weapon.normalAttack();
        }
        else{
            theAttackType = "charged attack";
            theAttack = weapon.chargedAttack();
        }
        logRepository.addLog(weapon.getHolderName()+" attacked with " +weaponName+ "("+theAttackType+"): "+theAttack);
        weaponRepository.save(weapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
