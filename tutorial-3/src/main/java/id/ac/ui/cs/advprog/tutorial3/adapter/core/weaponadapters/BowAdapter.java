package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class BowAdapter implements Weapon {

    private Bow bow;

    public BowAdapter(Bow bow) {
        this.bow = bow;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(false);
    }

    @Override
    public String chargedAttack() {
        return bow.shootArrow(true);
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return bow.getHolderName();
    }
}
