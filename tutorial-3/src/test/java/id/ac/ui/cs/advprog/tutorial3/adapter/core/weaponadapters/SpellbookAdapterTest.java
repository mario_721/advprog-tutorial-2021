package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Heatbearer;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.TheWindjedi;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

// TODO: add tests
public class SpellbookAdapterTest {
    private Class<?> spellbookAdapterClass;
    private Class<?> spellbookClass;

    @Autowired
    private SpellbookAdapter spellbookAdapter;

    @Mock
    private Spellbook spellbook;


    @BeforeEach
    public void setUp() throws Exception {
        spellbookAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter");
        spellbookClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook");
        spellbook = mock(Spellbook.class);
        spellbookAdapter = new SpellbookAdapter(spellbook);
    }

    @Test
    public void testSpellbookAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(spellbookAdapterClass.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(spellbookAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testSpellbookAdapterConstructorReceivesSpellbookAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = spellbookClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                spellbookAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testSpellbookAdapterOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = spellbookAdapterClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = spellbookAdapterClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideGetNameMethod() throws Exception {
        Method getName = spellbookAdapterClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideGetHolderMethod() throws Exception {
        Method getHolderName = spellbookAdapterClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // TODO: buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testSpellbookAdapterNormalAttackHeartbearerSmallSpell() throws Exception {
        Heatbearer heatbearer = new Heatbearer("Whatever");
        when(spellbook.smallSpell()).thenReturn(heatbearer.smallSpell());
        assertEquals(heatbearer.smallSpell(), spellbookAdapter.normalAttack());
    }

    @Test
    public void testSpellbookAdapterNormalAttackTheWindijediSmallSpell() throws Exception {
        TheWindjedi theWindjedi = new TheWindjedi("Whatever");
        when(spellbook.smallSpell()).thenReturn(theWindjedi.smallSpell());
        assertEquals(theWindjedi.smallSpell(), spellbookAdapter.normalAttack());
    }

    @Test
    public void testSpellbookAdapterNormalAttackHeartbearerLargeSpell() throws Exception {
        Heatbearer heatbearer = new Heatbearer("Whatever");
        when(spellbook.largeSpell()).thenReturn(heatbearer.largeSpell());
        assertEquals(heatbearer.largeSpell(), spellbookAdapter.chargedAttack());
    }

    @Test
    public void testSpellbookAdapterNormalAttackTheWindijediLargeSpell() throws Exception {
        TheWindjedi theWindjedi = new TheWindjedi("Whatever");
        when(spellbook.largeSpell()).thenReturn(theWindjedi.largeSpell());
        assertEquals(theWindjedi.largeSpell(), spellbookAdapter.chargedAttack());
    }

    @Test
    public void testSpellbookAdapterGetNameHeatbearer() throws Exception {
        Heatbearer heatbearer = new Heatbearer("Whatever");
        when(spellbook.getName()).thenReturn(heatbearer.getName());
        assertEquals("Heat Bearer", heatbearer.getName());
    }

    @Test
    public void testBowAdapterGetNameTheWindjedi() throws Exception {
        TheWindjedi theWindjedi = new TheWindjedi("Whatever");
        when(spellbook.getName()).thenReturn(theWindjedi.getName());
        assertEquals("The Windjedi", theWindjedi.getName());
    }

    @Test
    public void testSpellbookAdapterGetHolderName() throws Exception {
        Heatbearer heatbearer = new Heatbearer("Whatever");
        when(spellbook.getHolderName()).thenReturn(heatbearer.getHolderName());
        assertEquals("Whatever", heatbearer.getHolderName());
    }
}
