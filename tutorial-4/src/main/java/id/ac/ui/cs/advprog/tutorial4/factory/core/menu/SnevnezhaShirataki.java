package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiFactory;

public class SnevnezhaShirataki extends Menu {
    IngredientsFactory ingredientsFactory;
    //Ingridients:
    //Noodle: Shirataki
    //Meat: Fish
    //Topping: Flower
    //Flavor: Umami

    public SnevnezhaShirataki(String name){
        super(name);
        this.ingredientsFactory = new SnevnezhaShiratakiFactory();
        this.noodle = ingredientsFactory.createNoodle();
        this.meat = ingredientsFactory.createMeat();
        this.topping = ingredientsFactory.createTopping();
        this.flavor = ingredientsFactory.createFlavor();
    }
}