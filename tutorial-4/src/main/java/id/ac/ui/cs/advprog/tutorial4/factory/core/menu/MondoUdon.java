package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonFactory;

public class MondoUdon extends Menu {
    IngredientsFactory ingredientsFactory;
    //Ingridients:
    //Noodle: Udon
    //Meat: Chicken
    //Topping: Cheese
    //Flavor: Salty

    public MondoUdon(String name){
        super(name);
        this.ingredientsFactory = new MondoUdonFactory();
        this.noodle = ingredientsFactory.createNoodle();
        this.meat = ingredientsFactory.createMeat();
        this.topping = ingredientsFactory.createTopping();
        this.flavor = ingredientsFactory.createFlavor();
    }
}