package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaFactory;

public class LiyuanSoba extends Menu {
    IngredientsFactory ingredientsFactory;
    //Ingridients:
    //Noodle: Soba
    //Meat: Beef
    //Topping: Mushroom
    //Flavor: Sweet

    public LiyuanSoba(String name){
        super(name);
        this.ingredientsFactory = new LiyuanSobaFactory();
        this.noodle = ingredientsFactory.createNoodle();
        this.meat = ingredientsFactory.createMeat();
        this.topping = ingredientsFactory.createTopping();
        this.flavor = ingredientsFactory.createFlavor();
    }
}