package id.ac.ui.cs.advprog.tutorial4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"id.ac.ui.cs.advprog.tutorial4"})
public class Tutorial4Application {

    public static void main(String[] args) {
        SpringApplication.run(Tutorial4Application.class, args);
    }

}
