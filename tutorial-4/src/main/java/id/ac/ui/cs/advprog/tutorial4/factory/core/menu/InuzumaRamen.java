package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenFactory;

public class InuzumaRamen extends Menu {
    IngredientsFactory ingredientsFactory;
    //Ingridients:
    //Noodle: Ramen
    //Meat: Pork
    //Topping: Boiled Egg
    //Flavor: Spicy
    public InuzumaRamen(String name){
        super(name);
        this.ingredientsFactory = new InuzumaRamenFactory();
        this.noodle = ingredientsFactory.createNoodle();
        this.meat = ingredientsFactory.createMeat();
        this.topping = ingredientsFactory.createTopping();
        this.flavor = ingredientsFactory.createFlavor();
    }
}