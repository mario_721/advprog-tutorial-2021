package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiFactoryTest {
    private Class<?> snevnezhaShiratakiFactoryClass;

    @Autowired
    SnevnezhaShiratakiFactory snevnezhaShiratakiFactory;

    @Mock private Shirataki shirataki;
    @Mock private Flower flower;
    @Mock private Fish fish;
    @Mock private Umami umami;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiFactory");
        snevnezhaShiratakiFactory = new SnevnezhaShiratakiFactory();
    }

    @Test
    public void testSnevnezhaShiratakiFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(snevnezhaShiratakiFactoryClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory")));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateNoodle() throws Exception {
        SnevnezhaShiratakiFactory snevnezhaShiratakiFactorySpy = Mockito.spy(new SnevnezhaShiratakiFactory());
        Mockito.doReturn(shirataki).when(snevnezhaShiratakiFactorySpy).createNoodle();
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateMeat() throws Exception {
        SnevnezhaShiratakiFactory snevnezhaShiratakiFactorySpy = Mockito.spy(new SnevnezhaShiratakiFactory());
        Mockito.doReturn(fish).when(snevnezhaShiratakiFactorySpy).createMeat();
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateTopping() throws Exception {
        SnevnezhaShiratakiFactory snevnezhaShiratakiFactorySpy = Mockito.spy(new SnevnezhaShiratakiFactory());
        Mockito.doReturn(flower).when(snevnezhaShiratakiFactorySpy).createTopping();
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateFlavor() throws Exception {
        SnevnezhaShiratakiFactory snevnezhaShiratakiFactorySpy = Mockito.spy(new SnevnezhaShiratakiFactory());
        Mockito.doReturn(umami).when(snevnezhaShiratakiFactorySpy).createFlavor();
    }
}
