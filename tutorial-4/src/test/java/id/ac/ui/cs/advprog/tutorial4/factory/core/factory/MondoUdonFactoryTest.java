package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonFactoryTest {
    private Class<?> mondoUdonFactoryClass;

    @Autowired
    MondoUdonFactory mondoUdonFactory;

    @Mock private Udon udon;
    @Mock private Cheese cheese;
    @Mock private Chicken chicken;
    @Mock private Salty salty;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonFactory");
        mondoUdonFactory = new MondoUdonFactory();
    }

    @Test
    public void testMondoUdonFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mondoUdonFactoryClass.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory")));
    }

    @Test
    public void testMondoUdonFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = mondoUdonFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = mondoUdonFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = mondoUdonFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = mondoUdonFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryCreateNoodle() throws Exception {
        MondoUdonFactory mondoUdonFactorySpy = Mockito.spy(new MondoUdonFactory());
        Mockito.doReturn(udon).when(mondoUdonFactorySpy).createNoodle();
    }

    @Test
    public void testMondoUdonFactoryCreateMeat() throws Exception {
        MondoUdonFactory mondoUdonFactorySpy = Mockito.spy(new MondoUdonFactory());
        Mockito.doReturn(chicken).when(mondoUdonFactorySpy).createMeat();
    }

    @Test
    public void testMondoUdonFactoryCreateTopping() throws Exception {
        MondoUdonFactory mondoUdonFactorySpy = Mockito.spy(new MondoUdonFactory());
        Mockito.doReturn(cheese).when(mondoUdonFactorySpy).createTopping();
    }

    @Test
    public void testMondoUdonFactoryCreateFlavor() throws Exception {
        MondoUdonFactory mondoUdonFactorySpy = Mockito.spy(new MondoUdonFactory());
        Mockito.doReturn(salty).when(mondoUdonFactorySpy).createFlavor();
    }
}
