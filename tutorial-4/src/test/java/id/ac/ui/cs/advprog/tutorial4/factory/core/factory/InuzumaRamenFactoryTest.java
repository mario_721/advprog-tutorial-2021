package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenFactoryTest {
    private Class<?> inuzumaRamenFactoryClass;

    @Autowired
    InuzumaRamenFactory inuzumaRamenFactory;

    @Mock private Ramen ramen;
    @Mock private BoiledEgg boiledEgg;
    @Mock private Pork pork;
    @Mock private Spicy spicy;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenFactory");
        inuzumaRamenFactory = new InuzumaRamenFactory();
    }

    @Test
    public void testInuzumaRamenFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(inuzumaRamenFactoryClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(inuzumaRamenFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory")));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = inuzumaRamenFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = inuzumaRamenFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = inuzumaRamenFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = inuzumaRamenFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryCreateNoodle() throws Exception {
        InuzumaRamenFactory inuzumaRamenFactorySpy = Mockito.spy(new InuzumaRamenFactory());
        Mockito.doReturn(ramen).when(inuzumaRamenFactorySpy).createNoodle();
    }

    @Test
    public void testInuzumaRamenFactoryCreateMeat() throws Exception {
        InuzumaRamenFactory inuzumaRamenFactorySpy = Mockito.spy(new InuzumaRamenFactory());
        Mockito.doReturn(pork).when(inuzumaRamenFactorySpy).createMeat();
    }

    @Test
    public void testInuzumaRamenFactoryCreateTopping() throws Exception {
        InuzumaRamenFactory inuzumaRamenFactorySpy = Mockito.spy(new InuzumaRamenFactory());
        Mockito.doReturn(boiledEgg).when(inuzumaRamenFactorySpy).createTopping();
    }

    @Test
    public void testInuzumaRamenFactoryCreateFlavor() throws Exception {
        InuzumaRamenFactory inuzumaRamenFactorySpy = Mockito.spy(new InuzumaRamenFactory());
        Mockito.doReturn(spicy).when(inuzumaRamenFactorySpy).createFlavor();
    }
}
