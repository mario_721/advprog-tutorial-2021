package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class BeefTest {
    private Class<?> beefClass;

    @Autowired
    Beef beef;

    @BeforeEach
    public void setUp() throws Exception {
        beefClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef");
        beef = new Beef();
    }

    @Test
    public void testBeefIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(beefClass.getModifiers()));
    }

    @Test
    public void testBeefIsAnIngridients() {
        Collection<Type> interfaces = Arrays.asList(beefClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }

    @Test
    public void testBeefOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = beefClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertEquals("Adding Maro Beef Meat...",beef.getDescription());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }
}
