package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class UdonTest {
    private Class<?> udonClass;

    @Autowired
    Udon udon;

    @BeforeEach
    public void setUp() throws Exception {
        udonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon");
        udon = new Udon();
    }

    @Test
    public void testUdonIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(udonClass.getModifiers()));
    }

    @Test
    public void testUdonIsAnIngridients() {
        Collection<Type> interfaces = Arrays.asList(udonClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle")));
    }

    @Test
    public void testUdonOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = udonClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertEquals("Adding Mondo Udon Noodles...",udon.getDescription());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }
}
