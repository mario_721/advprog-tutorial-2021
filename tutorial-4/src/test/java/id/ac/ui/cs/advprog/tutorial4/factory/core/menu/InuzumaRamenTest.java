package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;

import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;

    @Autowired
    InuzumaRamen inuzumaRamen;

    @Mock
    private Ramen ramen;
    @Mock
    private BoiledEgg boiledEgg;
    @Mock
    private Pork pork;
    @Mock
    private Spicy spicy;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
        inuzumaRamen = new InuzumaRamen("Ramen Instan");
    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(inuzumaRamenClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIsAMenu() {
        Class<?> parentClass = inuzumaRamenClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                parentClass.getName());
    }

    @Test
    public void testInuzumaRamenGetNoodleReturnsRamen() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen",
                inuzumaRamen.getNoodle().getClass().getName());
            assertEquals("Adding Inuzuma Ramen Noodles...",
                inuzumaRamen.getNoodle().getDescription());
    }

    @Test
    public void testInuzumaRamenGetMeatReturnsPork() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork",
                inuzumaRamen.getMeat().getClass().getName());
        assertEquals("Adding Tian Xu Pork Meat...",
                inuzumaRamen.getMeat().getDescription());
    }

    @Test
    public void testInuzumaRamenGetFlavorReturnsSpicy() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy",
                inuzumaRamen.getFlavor().getClass().getName());
        assertEquals("Adding Liyuan Chili Powder...",
                inuzumaRamen.getFlavor().getDescription());
    }

    @Test
    public void testInuzumaRamenGetToppingReturnsBoiledEgg() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg",
                inuzumaRamen.getTopping().getClass().getName());
        assertEquals("Adding Guahuan Boiled Egg Topping",
                inuzumaRamen.getTopping().getDescription());
    }

}