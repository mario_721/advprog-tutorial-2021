package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaTest {
    private Class<?> liyuanSobaClass;

    @Autowired
    LiyuanSoba liyuanSoba;

    @Mock private Soba soba;
    @Mock private Mushroom mushroom;
    @Mock private Beef beef;
    @Mock private Sweet sweet;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
        liyuanSoba = new LiyuanSoba("Soba Instan");
    }

    @Test
    public void testLiyuanSobaIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(liyuanSobaClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIsAMenu() {
        Class<?> parentClass = liyuanSobaClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                parentClass.getName());
    }

    @Test
    public void testLiyuanSobaGetNoodleReturnsSoba() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba",
                liyuanSoba.getNoodle().getClass().getName());
            assertEquals("Adding Liyuan Soba Noodles...",
                liyuanSoba.getNoodle().getDescription());
    }

    @Test
    public void testLiyuanSobaGetMeatReturnsBeef() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef",
                liyuanSoba.getMeat().getClass().getName());
        assertEquals("Adding Maro Beef Meat...",
                liyuanSoba.getMeat().getDescription());
    }

    @Test
    public void testLiyuanSobaGetFlavorReturnsSweet() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet",
                liyuanSoba.getFlavor().getClass().getName());
        assertEquals("Adding a dash of Sweet Soy Sauce...",
                liyuanSoba.getFlavor().getDescription());
    }

    @Test
    public void testLiyuanSobaGetToppingReturnsMushroom() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom",
                liyuanSoba.getTopping().getClass().getName());
        assertEquals("Adding Shiitake Mushroom Topping...",
                liyuanSoba.getTopping().getDescription());
    }

}