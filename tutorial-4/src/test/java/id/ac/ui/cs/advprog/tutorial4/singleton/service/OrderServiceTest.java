package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {
    private Class<?> orderServiceClass;


    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
    }

    @Test
    public void testOrderServiceHasOrderADrinkMethod() throws Exception {
        Method orderADrink = orderServiceClass.getDeclaredMethod("orderADrink", String.class);
        assertTrue(Modifier.isPublic(orderADrink.getModifiers()));
        assertEquals(1, orderADrink.getParameterCount());
    }

    
    @Test
    public void testOrderServiceHasOrderAFoodMethod() throws Exception {
        Method orderAFood = orderServiceClass.getDeclaredMethod("orderAFood", String.class);
        assertTrue(Modifier.isPublic(orderAFood.getModifiers()));
        assertEquals(1, orderAFood.getParameterCount());
    }

    @Test
    public void testOrderServiceHasGetDrinksMethod() throws Exception {
        Method getDrink = orderServiceClass.getDeclaredMethod("getDrink");
        assertTrue(Modifier.isPublic(getDrink.getModifiers()));
        assertEquals(0, getDrink.getParameterCount());
    }

    
    @Test
    public void testOrderServiceHasGetFoodsMethod() throws Exception {
        Method getFood = orderServiceClass.getDeclaredMethod("getFood");
        assertTrue(Modifier.isPublic(getFood.getModifiers()));
        assertEquals(0, getFood.getParameterCount());
    }
}