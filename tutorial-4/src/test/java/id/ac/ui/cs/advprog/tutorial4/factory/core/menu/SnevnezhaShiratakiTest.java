package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiTest {
    private Class<?> snevnezhaShiratakiClass;

    @Autowired
    SnevnezhaShirataki snevnezhaShirataki;

    @Mock private Shirataki shirataki;
    @Mock private Flower flower;
    @Mock private Fish fish;
    @Mock private Umami umami;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
        snevnezhaShirataki = new SnevnezhaShirataki("Shirataki Instan");
    }

    @Test
    public void testSnevnezhaShiratakiIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(snevnezhaShiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIsAMenu() {
        Class<?> parentClass = snevnezhaShiratakiClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                parentClass.getName());
    }

    @Test
    public void testSnevnezhaShiratakiGetNoodleReturnsShirataki() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki",
                snevnezhaShirataki.getNoodle().getClass().getName());
            assertEquals("Adding Snevnezha Shirataki Noodles...",
                snevnezhaShirataki.getNoodle().getDescription());
    }

    @Test
    public void testSnevnezhaShiratakiGetMeatReturnsFish() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish",
                snevnezhaShirataki.getMeat().getClass().getName());
        assertEquals("Adding Zhangyun Salmon Fish Meat...",
                snevnezhaShirataki.getMeat().getDescription());
    }

    @Test
    public void testSnevnezhaShiratakiGetFlavorReturnsUmami() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami",
                snevnezhaShirataki.getFlavor().getClass().getName());
        assertEquals("Adding WanPlus Specialty MSG flavoring...",
                snevnezhaShirataki.getFlavor().getDescription());
    }

    @Test
    public void testSnevnezhaShiratakiGetToppingReturnsFlower() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower",
                snevnezhaShirataki.getTopping().getClass().getName());
        assertEquals("Adding Xinqin Flower Topping...",
                snevnezhaShirataki.getTopping().getDescription());
    }

}