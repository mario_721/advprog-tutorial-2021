package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class CheeseTest {
    private Class<?> cheeseClass;

    @Autowired
    Cheese cheese;

    @BeforeEach
    public void setUp() throws Exception {
        cheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese");
        cheese = new Cheese();
    }

    @Test
    public void testCheeseIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(cheeseClass.getModifiers()));
    }

    @Test
    public void testCheeseIsAnIngridients() {
        Collection<Type> interfaces = Arrays.asList(cheeseClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testCheeseOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = cheeseClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertEquals("Adding Shredded Cheese Topping...",cheese.getDescription());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }
}
