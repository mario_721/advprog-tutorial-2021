package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class SweetTest {
    private Class<?> sweetClass;

    @Autowired
    Sweet sweet;

    @BeforeEach
    public void setUp() throws Exception {
        sweetClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet");
        sweet = new Sweet();
    }

    @Test
    public void testSweetIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(sweetClass.getModifiers()));
    }

    @Test
    public void testSweetIsAnIngridients() {
        Collection<Type> interfaces = Arrays.asList(sweetClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testSweetOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = sweetClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertEquals("Adding a dash of Sweet Soy Sauce...",sweet.getDescription());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }
}
