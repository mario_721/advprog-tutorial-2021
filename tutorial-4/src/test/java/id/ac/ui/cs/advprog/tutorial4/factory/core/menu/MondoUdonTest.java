package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;

import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonTest {
    private Class<?> mondoUdonClass;

    @Autowired
    MondoUdon mondoUdon;

    @Mock private Udon udon;
    @Mock private Cheese cheese;
    @Mock private Chicken chicken;
    @Mock private Salty salty;


    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
        mondoUdon = new MondoUdon("Udon Instan");
    }

    @Test
    public void testMondoUdonIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mondoUdonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonIsAMenu() {
        Class<?> parentClass = mondoUdonClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                parentClass.getName());
    }

    @Test
    public void testMondoUdonGetNoodleReturnsUdon() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon",
                mondoUdon.getNoodle().getClass().getName());
            assertEquals("Adding Mondo Udon Noodles...",
                mondoUdon.getNoodle().getDescription());
    }

    @Test
    public void testMondoUdonGetMeatReturnsChicken() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken",
                mondoUdon.getMeat().getClass().getName());
        assertEquals("Adding Wintervale Chicken Meat...",
                mondoUdon.getMeat().getDescription());
    }

    @Test
    public void testMondoUdonGetFlavorReturnsSalty() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty",
                mondoUdon.getFlavor().getClass().getName());
        assertEquals("Adding a pinch of salt...",
                mondoUdon.getFlavor().getDescription());
    }

    @Test
    public void testMondoUdonGetToppingReturnsCheese() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese",
                mondoUdon.getTopping().getClass().getName());
        assertEquals("Adding Shredded Cheese Topping...",
                mondoUdon.getTopping().getDescription());
    }

}