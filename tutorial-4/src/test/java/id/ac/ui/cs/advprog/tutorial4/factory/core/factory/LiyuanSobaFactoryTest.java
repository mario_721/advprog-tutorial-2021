package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaFactoryTest {
    private Class<?> liyuanSobaFactoryClass;

    @Autowired
    LiyuanSobaFactory liyuanSobaFactory;

    @Mock private Soba soba;
    @Mock private Mushroom mushroom;
    @Mock private Beef beef;
    @Mock private Sweet sweet;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaFactory");
        liyuanSobaFactory = new LiyuanSobaFactory();
    }

    @Test
    public void testLiyuanSobaFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(liyuanSobaFactoryClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory")));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = liyuanSobaFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = liyuanSobaFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = liyuanSobaFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = liyuanSobaFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryCreateNoodle() throws Exception {
        LiyuanSobaFactory liyuanSobaFactorySpy = Mockito.spy(new LiyuanSobaFactory());
        Mockito.doReturn(soba).when(liyuanSobaFactorySpy).createNoodle();
    }

    @Test
    public void testLiyuanSobaFactoryCreateMeat() throws Exception {
        LiyuanSobaFactory liyuanSobaFactorySpy = Mockito.spy(new LiyuanSobaFactory());
        Mockito.doReturn(beef).when(liyuanSobaFactorySpy).createMeat();
    }

    @Test
    public void testLiyuanSobaFactoryCreateTopping() throws Exception {
        LiyuanSobaFactory liyuanSobaFactorySpy = Mockito.spy(new LiyuanSobaFactory());
        Mockito.doReturn(mushroom).when(liyuanSobaFactorySpy).createTopping();
    }

    @Test
    public void testLiyuanSobaFactoryCreateFlavor() throws Exception {
        LiyuanSobaFactory liyuanSobaFactorySpy = Mockito.spy(new LiyuanSobaFactory());
        Mockito.doReturn(sweet).when(liyuanSobaFactorySpy).createFlavor();
    }
}
