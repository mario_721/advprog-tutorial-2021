package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class OrderFoodTest {
    private Class<?> orderFoodClass;

    @Mock private OrderFood orderFood;

    @BeforeEach
    public void setup() throws Exception {
        orderFoodClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood");
    }

    @Test
    public void testOrderFoodHasGetInstance() throws Exception {
        Method getInstance = orderFoodClass.getDeclaredMethod("getInstance");
        assertTrue(Modifier.isPublic(getInstance.getModifiers()));
        assertEquals(0, getInstance.getParameterCount());
    }

    
    @Test
    public void testGetInstanceCorrectlyImplemented() {
        assertNotEquals(null, OrderFood.getInstance());
    }

    @Test
    public void testOrderFoodHasSetFoodMethod() throws Exception {
        Method setFood = orderFoodClass.getDeclaredMethod("setFood", String.class);
        assertTrue(Modifier.isPublic(setFood.getModifiers()));
        assertEquals(1, setFood.getParameterCount());
    }

    @Test
    public void testOrderFoodHasGetFoodsMethod() throws Exception {
        Method getFood = orderFoodClass.getDeclaredMethod("getFood");
        assertTrue(Modifier.isPublic(getFood.getModifiers()));
        assertEquals(0, getFood.getParameterCount());
    }

    @Test
    public void testOrderFoodHasToString() throws Exception {
        Method toString = orderFoodClass.getDeclaredMethod("toString");
        assertTrue(Modifier.isPublic(toString.getModifiers()));
        assertEquals(0, toString.getParameterCount());
    }

    @Test
    public void testSetAndGetFoodMethod() throws Exception {
        orderFood = OrderFood.getInstance();
        orderFood.setFood("Tonkotsu Ramen");
        assertEquals("Tonkotsu Ramen", orderFood.getFood());
    }

}