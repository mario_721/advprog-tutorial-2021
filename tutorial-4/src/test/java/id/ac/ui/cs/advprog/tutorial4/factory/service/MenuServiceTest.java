package id.ac.ui.cs.advprog.tutorial4.factory.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MenuServiceTest {
    private Class<?> menuServiceClass;

    @Mock
    private MenuRepository menuRepository;

    @InjectMocks
    private MenuServiceImpl menuService;

    @BeforeEach
    public void setup() throws Exception {
        menuServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
    }

    @Test
    public void testMenuServiceHasCreateMenuMethod() throws Exception {
        Method createMenu = menuServiceClass.getDeclaredMethod("createMenu", String.class, String.class);
        assertTrue(Modifier.isPublic(createMenu.getModifiers()));
        assertEquals(2, createMenu.getParameterCount());
    }

    @Test
    public void testMenuServiceHasGetMenusMethod() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");
        assertTrue(Modifier.isPublic(getMenus.getModifiers()));
        assertEquals(0, getMenus.getParameterCount());
    }

    @Test
    public void testWhenGetMenusIsCalledItShouldCallShowAllMenus() {
        Menu menu = new InuzumaRamen("Ramen Instan");
        List<Menu> menuList = new ArrayList<>();
        menuList.add(menu);
        when(menuService.getMenus())

			.thenReturn(menuList);

        Iterable<Menu> calledMenuList = menuService.getMenus();

        verify(menuRepository, times(1)).getMenus();
        assertThat(calledMenuList.iterator().next())

			.isEqualTo(menuList.get(0));
    }
}