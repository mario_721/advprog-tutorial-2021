package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class MenuRepositoryTest {
    private Class<?> menuRepositoryClass;

    @InjectMocks
    private MenuRepository menuRepository;

    @BeforeEach
    public void setup() throws Exception {
        menuRepositoryClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository");
        menuRepository = new MenuRepository();
    }

    @Test
    public void testMenuRepositoryHasAddMethod() throws Exception {
        Method add = menuRepositoryClass.getDeclaredMethod("add", Menu.class);
        assertTrue(Modifier.isPublic(add.getModifiers()));
        assertEquals(1, add.getParameterCount());
    }

    @Test
    public void testMenuRepositoryOnAddNewObject() throws Exception {
        Menu menuInstance = new InuzumaRamen("ramen instan");
        Method add = menuRepositoryClass.getDeclaredMethod("add", Menu.class);

        Menu addedMenu = (Menu) add.invoke(new MenuRepository(), menuInstance);

        assertEquals(menuInstance, addedMenu);
    }

    @Test
    public void testMenuRepositoryHasGetMenusMethod() throws Exception {
        Method getMenus = menuRepositoryClass.getDeclaredMethod("getMenus");
        assertTrue(Modifier.isPublic(getMenus.getModifiers()));
        assertEquals(0, getMenus.getParameterCount());
    }

    @Test
    public void testMenuRepositoryOnGettingAllMenus() {
        menuRepository.add(new InuzumaRamen("ramen instan"));
        menuRepository.add(new LiyuanSoba("soba instan"));
        
        List<Menu> allMenu = menuRepository.getMenus();
        assertThat(allMenu.size()).isEqualTo(2);
    }
}