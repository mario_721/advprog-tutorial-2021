package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IngredientsFactoryTest {
    private Class<?> ingredientsFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        ingredientsFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory");
    }

    @Test
    public void testIngredientsFactoryIsAPublicInterface() {
        int classModifiers = ingredientsFactoryClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testIngredientsFactoryHasCreateNoodleAbstractMethod() throws Exception {
        Method createNoodle = ingredientsFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createNoodle.getParameterCount());
    }

    @Test
    public void testIngredientsFactoryHasCreateMeatAbstractMethod() throws Exception {
        Method createMeat = ingredientsFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createMeat.getParameterCount());
    }

    @Test
    public void testIngredientsFactoryHasCreateToppingAbstractMethod() throws Exception {
        Method createTopping = ingredientsFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createTopping.getParameterCount());
    }

    @Test
    public void testIngredientsFactoryHasCreateFlavorAbstractMethod() throws Exception {
        Method createFlavor = ingredientsFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createFlavor.getParameterCount());
    }
}
