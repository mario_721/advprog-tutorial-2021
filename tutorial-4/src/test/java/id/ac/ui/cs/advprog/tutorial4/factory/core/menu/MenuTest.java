package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuTest {
    private Class<?> menuClass;

    @BeforeEach
    public void setup() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testMenuIsAbstract() {
        assertTrue(Modifier.isAbstract(menuClass.getModifiers()));
    }

    @Test
    public void testMenuHasGetNoodle() throws Exception {
        Method getNoodle = menuClass.getDeclaredMethod("getNoodle");
        assertTrue(Modifier.isPublic(getNoodle.getModifiers()));
        assertEquals(0, getNoodle.getParameterCount());
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle", getNoodle.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMenuHasGetMeat() throws Exception {
        Method getMeat = menuClass.getDeclaredMethod("getMeat");
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
        assertEquals(0, getMeat.getParameterCount());
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat", getMeat.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMenuHasGetTopping() throws Exception {
        Method getTopping = menuClass.getDeclaredMethod("getTopping");
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
        assertEquals(0, getTopping.getParameterCount());
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping", getTopping.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMenuHasGetFlavor() throws Exception {
        Method getFlavor = menuClass.getDeclaredMethod("getFlavor");
        assertTrue(Modifier.isPublic(getFlavor.getModifiers()));
        assertEquals(0, getFlavor.getParameterCount());
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor", getFlavor.getGenericReturnType().getTypeName());
    }
}
