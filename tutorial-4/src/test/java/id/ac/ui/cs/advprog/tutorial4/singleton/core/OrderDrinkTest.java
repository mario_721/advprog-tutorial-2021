package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class OrderDrinkTest {
    private Class<?> orderDrinkClass;

    @Mock private OrderDrink orderDrink;

    @BeforeEach
    public void setup() throws Exception {
        orderDrinkClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink");
        orderDrink = null;
    }
    
    @Test
    public void testOrderDrinkHasGetInstance() throws Exception {
        Method getInstance = orderDrinkClass.getDeclaredMethod("getInstance");
        assertTrue(Modifier.isPublic(getInstance.getModifiers()));
        assertEquals(0, getInstance.getParameterCount());
    }

    @Test
    public void testGetInstanceCorrectlyImplemented() {
        assertNotEquals(null, OrderDrink.getInstance());
    }

    @Test
    public void testOrderDrinkHasSetDrinkMethod() throws Exception {
        Method setDrink = orderDrinkClass.getDeclaredMethod("setDrink", String.class);
        assertTrue(Modifier.isPublic(setDrink.getModifiers()));
        assertEquals(1, setDrink.getParameterCount());
    }

    @Test
    public void testOrderDrinkHasGetDrinksMethod() throws Exception {
        Method getDrink = orderDrinkClass.getDeclaredMethod("getDrink");
        assertTrue(Modifier.isPublic(getDrink.getModifiers()));
        assertEquals(0, getDrink.getParameterCount());
    }

    @Test
    public void testOrderDrinkHasToString() throws Exception {
        Method toString = orderDrinkClass.getDeclaredMethod("toString");
        assertTrue(Modifier.isPublic(toString.getModifiers()));
        assertEquals(0, toString.getParameterCount());
    }

    @Test
    public void testSetAndGetDrinkMethod() throws Exception {
        orderDrink = OrderDrink.getInstance();
        orderDrink.setDrink("Bir Bintang");
        assertEquals("Bir Bintang", orderDrink.getDrink());
    }

}