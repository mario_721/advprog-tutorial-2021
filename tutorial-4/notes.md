# Apa Perbedaan masing-masing approach dalam penggunaan Singleton Pattern

##1. Eager Initialization
Pada eager initialization, instance dari class singleton dibuat langsung pada saat class itu diakses.

Kelebihan:
- Eager initialization adalah metode paling mudah dalam pembuatan singleton class
- Cocok digunakan jika class singleton tidak menggunakan banyak resources

Kekurangan:
- Instance akan tetap dibuat bahkan jika tidak digunakan
- Eager initialization tidak menyediakan pilihan untuk exception handling


Lazy initialization method to implement Singleton pattern creates the instance in the global access method.

The above implementation works fine in case of the single-threaded environment but when it comes to
multithreaded systems, it can cause issues if multiple threads are inside the if condition at the same time.
It will destroy the singleton pattern and both threads will get the different instances of the singleton class.
In next section, we will see different ways to create a thread-safe singleton class.

##2. Lazy Initialization
Pada lazy initialization, instance dibuat dalam sebuah method yang memiliki global access.

Kelebihan:
- Dapat bekerja dengan baik dalam single-threaded environment

Kekurangan:
- Dalam multithreaded systems, lazy initialization dapat mengakibatkan banyak masalah. Bahkan dapat menghancurkan singleton pattern itu sendiri.

###Referensi
[journaldev.com](https://www.journaldev.com/1377/java-singleton-design-pattern-best-practices-examples)